import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import repositoryImpl.ArrowRepositoryImpl;
import repositoryImpl.BranchRepositoryImpl;
import repositoryImpl.RightsRepositoryImpl;
import repositoryImpl.RouteRepositoryImpl;
import repositoryImpl.StationRepositoryImpl;
import repositoryImpl.TrafficLightRepositoryImpl;
import repositoryImpl.TrainRepositoryImpl;
import repositoryImpl.TunnelRepositoryImpl;
import repositoryImpl.UserRepositoryImpl;
import scenarios.Scenario;
import type.Arrow;
import type.Location;
import type.Station;
import type.TrafficLight;

import java.util.ArrayList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ScenarioTest {

    Scenario scenario;

    public ScenarioTest() {
    }

    @BeforeAll
    public void generate() throws Exception{
        scenario = new Scenario(new ArrowRepositoryImpl(),
                new BranchRepositoryImpl(),
                new RightsRepositoryImpl(),
                new RouteRepositoryImpl(),
                new StationRepositoryImpl(),
                new TrafficLightRepositoryImpl(),
                new TrainRepositoryImpl(),
                new TunnelRepositoryImpl(),
                new UserRepositoryImpl());
        scenario.rightsRepositoryImpl.generate();
        scenario.arrowRepositoryImpl.generate();
        scenario.trafficLightRepositoryImpl.generate();
        scenario.userRepositoryImpl.generate();
        scenario.trainRepository.generate();
        scenario.tunnelRepositoryImpl.generate();
        scenario.stationRepositoryImpl.generate();
        scenario.routeRepositoryImpl.generate();
        scenario.branchRepositoryImpl.generate();
    }


    @AfterAll
    public void clear() throws Exception{
        scenario.rightsRepositoryImpl.clear();
        scenario.arrowRepositoryImpl.clear();
        scenario.trafficLightRepositoryImpl.clear();
        scenario.userRepositoryImpl.clear();
        scenario.trainRepository.clear();
        scenario.tunnelRepositoryImpl.clear();
        scenario.stationRepositoryImpl.clear();
        scenario.routeRepositoryImpl.clear();
        scenario.branchRepositoryImpl.clear();
    }

    @Test
    public void getStateOfTrafficLightAroundStationTest(){
            scenario.getStateOfTrafficLightAroundStation(1);
    }

    @Test
    void getStateOfTrafficLightAroundStationTestFail1() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.getStateOfTrafficLightAroundStation(null);
        });
    }
    @Test
    public void getStateOfTrafficLightAroundStationTestFail2(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.getStateOfTrafficLightAroundStation(33333);
        });
    }

    @Test
    public void addArrowTest(){
            scenario.addArrow(500,1);
    }

    @Test
    void addArrowTestFail1() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.addArrow(500,null);
        });
    }

    @Test
    void addArrowTestFail2() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.addArrow(3,1);
        });
    }

    @Test
    public void deleteArrowTest(){
        Assert.assertEquals(scenario.deleteArrow(1),true);
    }

    @Test
    void deleteArrowTest1() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.deleteArrow(null);
        });
    }

    @Test
    void deleteArrowTest2() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.deleteArrow(345678);
        });
    }

    @Test
    public void addTrafficLightTest(){
            scenario.addTrafficLight(500,true);
    }

    @Test
    public void addTrafficLightTestFail1(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.addTrafficLight(2,true);
        });
    }

    @Test
    public void addTrafficLightTestFail2(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.addTrafficLight(5,null);
        });
    }

    @Test
    public void deleteTrafficLightTest(){
        scenario.deleteTrafficLight(4);
    }

    @Test
    public void deleteTrafficLightTestFail1(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.deleteTrafficLight(null);
        });
    }

    @Test
    public void deleteTrafficLightTestFail2(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.deleteTrafficLight(5345678);
        });
    }

    @Test
    public void getWorkloadOfAllTunnelsTest(){
        scenario.getWorkloadOfAllTunnels();
    }


    @Test
    public void setDirectionOfArrowTest()
    {
        scenario.setDirectionOfArrow(2,"Main",1);
    }

    @Test
    public void setDirectionOfArrowTestFail1(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.setDirectionOfArrow(23456789,"Main",1);
        });
    }
    @Test
    public void setDirectionOfArrowTestFail2(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.setDirectionOfArrow(2,"dfdddddddddd",1);
        });
    }
    @Test
    public void setDirectionOfArrowTestFail3(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.setDirectionOfArrow(2,"Main",null);
        });
    }


    @Test
    public void setStateOfTrafficLightTest(){
        scenario.setStateOfTrafficLight(3,"Main",true);
    }

    @Test
    public void setStateOfTrafficLightTestFail1(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.setStateOfTrafficLight(555,"Main",true);
        });
    }
    @Test
    public void setStateOfTrafficLightTestFail2(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.setStateOfTrafficLight(2,"dfdddddddddd",true);
        });
    }
    @Test
    public void setStateOfTrafficLightTestFail3(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.setStateOfTrafficLight(2,"Main",null);
        });
    }


    @Test
    public void deleteRightsForArrowForUserTest(){
        scenario.deleteRightsForArrowForUser("Main",3);
    }

    @Test
    public void deleteRightsForArrowForUserTestFail1(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.deleteRightsForArrowForUser(null,1);
        });
    }

    @Test
    public void deleteRightsForArrowForUserTestFail2(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.deleteRightsForArrowForUser("dfghjkil",2);
        });
    }

    @Test
    public void deleteRightsForArrowForUserTestFail3(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.deleteRightsForArrowForUser("Main",444);
        });
    }

    @Test
    public void deleteRightsForTrafficLightForUserTest(){
        scenario.deleteRightsForTrafficLightForUser("Main",2);
    }


    @Test
    public void deleteRightsForTrafficLightForUserTestFail1(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.deleteRightsForTrafficLightForUser(null,1);
        });
    }

    @Test
    public void deleteRightsForTrafficLightForUserTestFail2(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.deleteRightsForTrafficLightForUser("dfghjkil",2);
        });
    }

    @Test
    public void deleteRightsForTrafficLightForUserTestFail3(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.deleteRightsForTrafficLightForUser("Main",4644);
        });
    }
    @Test
    public void addRightsForTrafficLightForUserTest(){
        scenario.addRightsForTrafficLightForUser("Main",2);
    }

    @Test
    public void addRightsForTrafficLightForUserTestFail1(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.addRightsForTrafficLightForUser(null,1);
        });
    }

    @Test
    public void addRightsForTrafficLightForUserTestFail2(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.addRightsForTrafficLightForUser("dfghjkil",2);
        });
    }

    @Test
    public void addRightsForTrafficLightForUserTestFail3(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.addRightsForTrafficLightForUser("Main",34444);
        });
    }

    @Test
    public void addRightsForArrowForUserTest(){
        scenario.addRightsForArrowForUser("Cool",2);
    }
    @Test
    public void getTrafficLightsAroundBranchTest(){
        scenario.getStateOfTrafficLightsAroundBranch(1);
    }

    @Test
    public void getDirectionsOfArrowsAroundBranchTest(){
        scenario.getDirectionsOfArrowsAroundBranch(1);
    }

    @Test
    public void getWorkloadOfTunnelTest(){

        scenario.getWorkloadOfTunnel(2);
    }

    @Test
    public void getArrowsAroundStationTest(){
        scenario.getArrowsAroundStation(1);
    }

    @Test
    public void getTrafficLightAroundStationTest(){
        scenario.getTrafficLightAroundStation(1);
    }

    @Test
    public void getLocationOfTrainTest(){
        Arrow arrow1=new Arrow(1,1);
        Arrow arrow2=new Arrow(2,2);
        Arrow arrow3=new Arrow(3,1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1=new TrafficLight(1,false);
        TrafficLight trafficLight2=new TrafficLight(2,false);
        TrafficLight trafficLight3=new TrafficLight(3,true);
        TrafficLight trafficLight4=new TrafficLight(4,false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        Station s1= new Station(1,arrows1,trafficLights1);
        Location l1 = new Location(s1);
        Assert.assertEquals(scenario.getLocationOfTrain(1),l1.getPoint());
    }


    @Test
    public void getStateOfTrafficLightTest(){
            System.out.println(scenario.getStateOfTrafficLight(1));
    }




    @Test
    public void getDirectionOfArrowTest(){
            scenario.getDirectionOfArrow(1);
    }

    @Test
    public void getRouteOfTrainTest(){
           scenario.getRouteOfTrain(1);
    }

    @Test
    public void getDirectionOfArrowsAroundStationTest(){
            scenario.getDirectionOfArrowsAroundStation(1);
    }

    @Test
    public void getDirectionOfArrowsAroundStationTestFail(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            scenario.getDirectionOfArrowsAroundStation(null);
        });
    }

}
