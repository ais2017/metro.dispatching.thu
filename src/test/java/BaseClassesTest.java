import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import type.Arrow;
import type.Branch;
import type.Location;
import type.Platform;
import type.Point;
import type.Rights;
import type.Route;
import type.Station;
import type.TrafficLight;
import type.Tunnel;
import type.User;
import type.Waypoint;

import java.util.ArrayList;
import java.util.Date;

public class BaseClassesTest {

    @Test
    public void StationTest1(){
        Integer i =1;
        Arrow arrow1=new Arrow(1,1);
        Arrow arrow2=new Arrow(2,2);
        Arrow arrow3=new Arrow(3,1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1=new TrafficLight(1,false);
        TrafficLight trafficLight2=new TrafficLight(2,false);
        TrafficLight trafficLight3=new TrafficLight(3,true);
        TrafficLight trafficLight4=new TrafficLight(4,false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        trafficLights1.add(trafficLight1);
        trafficLights1.add(trafficLight2);
        trafficLights1.add(trafficLight3);
        trafficLights1.add(trafficLight4);
        Station s1= new Station(1,arrows1,trafficLights1);
        Assert.assertEquals(s1.getId(),i);
        Assert.assertEquals(s1.getArrows(),arrows1);
        Assert.assertEquals(s1.getTrafficLights(),trafficLights1);
    }

    @Test
    public void StationTest12(){
        Integer i =1;
        Arrow arrow1=new Arrow(1,1);
        Arrow arrow2=new Arrow(2,2);
        Arrow arrow3=new Arrow(3,2);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1=new TrafficLight(1,false);
        TrafficLight trafficLight2=new TrafficLight(2,false);
        TrafficLight trafficLight3=new TrafficLight(3,true);
        TrafficLight trafficLight4=new TrafficLight(4,false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        trafficLights1.add(trafficLight1);
        trafficLights1.add(trafficLight2);
        trafficLights1.add(trafficLight3);
        trafficLights1.add(trafficLight4);
        Station s1= new Station(1,arrows1,trafficLights1);
        Arrow arrow4=new Arrow(4,1);
        s1.addArrow(arrow4);
    }

    @Test
    public void StationTestFail2(){
        Integer i =1;
        Arrow arrow1=new Arrow(1,1);
        Arrow arrow2=new Arrow(2,2);
        Arrow arrow3=new Arrow(3,2);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1=new TrafficLight(1,false);
        TrafficLight trafficLight2=new TrafficLight(2,false);
        TrafficLight trafficLight3=new TrafficLight(3,true);
        TrafficLight trafficLight4=new TrafficLight(4,false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        trafficLights1.add(trafficLight1);
        trafficLights1.add(trafficLight2);
        trafficLights1.add(trafficLight3);
        trafficLights1.add(trafficLight4);
        Station s1= new Station(1,arrows1,trafficLights1);
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            s1.addArrow(null);
        });
    }

    @Test
    public void StationTestFail3(){
        Integer i =1;
        Arrow arrow1=new Arrow(1,1);
        Arrow arrow2=new Arrow(2,2);
        Arrow arrow3=new Arrow(3,2);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1=new TrafficLight(1,false);
        TrafficLight trafficLight2=new TrafficLight(2,false);
        TrafficLight trafficLight3=new TrafficLight(3,true);
        TrafficLight trafficLight4=new TrafficLight(4,false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        trafficLights1.add(trafficLight1);
        trafficLights1.add(trafficLight2);
        trafficLights1.add(trafficLight3);
        trafficLights1.add(trafficLight4);
        Station s1= new Station(1,arrows1,trafficLights1);
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            s1.addTrafficLight(null);
        });
    }

    @Test
    public void StationTestFail(){
        Tunnel tt1 = null;
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Station(null,null);
        });
    }

    @Test
    public void ArrowTest1(){
        Integer i = 1;
        Integer j = 2;
        Arrow arrow1=new Arrow(1,2);
        Assert.assertEquals(arrow1.getId(),i);
        Assert.assertEquals(arrow1.getDirection(),j);
    }

    @Test
    public void ArrowTestFail(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Arrow(2,6);
        });
    }


    @Test
    public void BranchTest1(){
        Integer id1=1;
        Integer type=2;
        Arrow arrow1 = new Arrow(1, 1);
        Arrow arrow2 = new Arrow(2, 2);
        Arrow arrow3 = new Arrow(3, 1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1 = new TrafficLight(1, false);
        TrafficLight trafficLight2 = new TrafficLight(2, false);
        TrafficLight trafficLight3 = new TrafficLight(3, true);
        TrafficLight trafficLight4 = new TrafficLight(4, false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        Station s1 = new Station(1, arrows1, trafficLights1);
        Tunnel tt1 = new Tunnel(1);
        ArrayList<Point> p1 = new ArrayList<Point>();
        p1.add(s1);
        p1.add(tt1);
        Branch b1 = new Branch(1,p1,2);
        Assert.assertEquals(b1.getId(),id1);
        Assert.assertEquals(b1.getTypeBranch(),type);
        Assert.assertEquals(b1.getPoints(),p1);
    }


    @Test
    public void BranchTestFail(){
        Arrow arrow1 = new Arrow(1, 1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        TrafficLight trafficLight1 = new TrafficLight(1, false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        Station s1 = new Station(1, arrows1, trafficLights1);
        Tunnel tt1 = new Tunnel(1);
        ArrayList<Point> p1 = new ArrayList<Point>();
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Branch(12,p1,8);
        });
    }

    @Test
    public void RightsTest1(){
        Arrow arrow1=new Arrow(1,1);
        Arrow arrow2=new Arrow(2,2);
        Arrow arrow3=new Arrow(3,1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        ArrayList<Arrow> arrows2 = new ArrayList<Arrow>();
        arrows2.add(arrow1);
        arrows2.add(arrow2);
        ArrayList<Arrow> arrows3 = new ArrayList<Arrow>();
        arrows3.add(arrow1);

        TrafficLight trafficLight1=new TrafficLight(1,false);
        TrafficLight trafficLight2=new TrafficLight(2,false);
        TrafficLight trafficLight3=new TrafficLight(3,true);
        TrafficLight trafficLight4=new TrafficLight(4,false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        trafficLights1.add(trafficLight1);
        trafficLights1.add(trafficLight2);
        trafficLights1.add(trafficLight3);
        ArrayList<TrafficLight> trafficLights2 = new ArrayList<TrafficLight>();
        trafficLights2.add(trafficLight1);
        trafficLights2.add(trafficLight2);
        ArrayList<TrafficLight> trafficLights3 = new ArrayList<TrafficLight>();
        trafficLights3.add(trafficLight4);
        User u = new User("X",true);
        Rights r = new Rights(u,arrows1, trafficLights1);
        Assert.assertEquals(r.getUser(),u);
        Assert.assertEquals(r.getArrows(),arrows1);
        Assert.assertEquals(r.getTrafficLights(),trafficLights1);
    }

    @Test
    public void RightsTestFail(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Rights(null,null,null);
        });
    }


    @Test
    public void LocationTest1(){
        Tunnel tt1 = new Tunnel(1);
        Location l = new Location(tt1);
        Assert.assertEquals(l.getPoint(),tt1);
    }

    @Test
    public void LocationTestFail(){
        Tunnel tt1 = null;
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Location(tt1);
        });
    }

    @Test
    public void PlatformTest1(){
        Integer i=1;
        Platform p = new Platform(1);
        Assert.assertEquals(p.getNumb(),i);
    }

    @Test
    public void PlatformTestFail(){
        Tunnel tt1 = null;
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Platform(678);
        });
    }

    @Test
    public void RouteTest1(){
        Arrow arrow1 = new Arrow(1, 1);
        Arrow arrow2 = new Arrow(2, 2);
        Arrow arrow3 = new Arrow(3, 1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1 = new TrafficLight(1, false);
        TrafficLight trafficLight2 = new TrafficLight(2, false);
        TrafficLight trafficLight3 = new TrafficLight(3, true);
        TrafficLight trafficLight4 = new TrafficLight(4, false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        Station s1 = new Station(1, arrows1, trafficLights1);
        Tunnel tt1 = new Tunnel(1);
        Waypoint w1 = new Waypoint(s1, new Date());
        Waypoint w2 = new Waypoint(tt1, new Date());
        ArrayList<Waypoint> aw1 = new ArrayList<Waypoint>();
        aw1.add(w1);
        aw1.add(w2);
        Route r1 = new Route(aw1);
        Assert.assertEquals(r1.getWaypoints(),aw1);
    }

    @Test
    public void RouteTest2(){
        Arrow arrow1 = new Arrow(1, 1);
        Arrow arrow2 = new Arrow(2, 2);
        Arrow arrow3 = new Arrow(3, 1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1 = new TrafficLight(1, false);
        TrafficLight trafficLight2 = new TrafficLight(2, false);
        TrafficLight trafficLight3 = new TrafficLight(3, true);
        TrafficLight trafficLight4 = new TrafficLight(4, false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        Station s1 = new Station(1, arrows1, trafficLights1);
        Tunnel tt1 = new Tunnel(1);
        Waypoint w1 = new Waypoint(s1, new Date());
        Waypoint w2 = new Waypoint(tt1, new Date());
        ArrayList<Waypoint> aw1 = new ArrayList<Waypoint>();
        aw1.add(w1);
        aw1.add(w2);
        Route r1 = new Route(aw1);
        Waypoint w3 = new Waypoint(s1, new Date());
        r1.addWaypoint(w3);
    }

    @Test
    public void RouteTest3(){
        Arrow arrow1 = new Arrow(1, 1);
        Arrow arrow2 = new Arrow(2, 2);
        Arrow arrow3 = new Arrow(3, 1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1 = new TrafficLight(1, false);
        TrafficLight trafficLight2 = new TrafficLight(2, false);
        TrafficLight trafficLight3 = new TrafficLight(3, true);
        TrafficLight trafficLight4 = new TrafficLight(4, false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        Station s1 = new Station(1, arrows1, trafficLights1);
        Tunnel tt1 = new Tunnel(1);
        Waypoint w1 = new Waypoint(s1, new Date());
        Waypoint w2 = new Waypoint(tt1, new Date());
        ArrayList<Waypoint> aw1 = new ArrayList<Waypoint>();
        aw1.add(w1);
        aw1.add(w2);
        Route r1 = new Route(aw1);
        r1.deleteWaypoint(w1);
    }

    @Test
    public void RouteTestFail(){
        ArrayList<Waypoint> a = null;
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Route(a);
        });
    }
    @Test
    public void TrafficLightTest1(){
        Integer i=1;
        TrafficLight t = new TrafficLight(1,true);
        Assert.assertEquals(t.getId(),i);
        Assert.assertEquals(t.isState(),true);
    }

    @Test
    public void TrafficLightFail(){
        Tunnel tt1 = null;
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new TrafficLight(null,null);
        });
    }

    @Test
    public void UserTest1(){
        User u = new User("X",true);
        Assert.assertEquals(u.getName(),"X");
        Assert.assertEquals(u.isReadyToReplacement(),true);
    }

    @Test
    public void UserFail(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new User(null,null);
        });
    }

    @Test
    public void WaypointTest1(){
        Tunnel tt1 = new Tunnel(1);
        Waypoint w = new Waypoint(tt1,new Date());
        Assert.assertEquals(w.getPoint(),tt1);
        Assert.assertEquals(w.getTimeOfPoint(),new Date());
    }

    @Test
    public void  WaypointFail(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            new Waypoint(null,null);
        });
    }

}