package repository;

import java.util.ArrayList;
import java.util.Collection;

public interface BranchRepository<T,M> {

    public void save(T t);

    public Collection<T> getAll();

    public T getOne(Integer id);

    public ArrayList<M> getPointsOfBranch(Integer id);

    public void generate();

    public void clear();
}
