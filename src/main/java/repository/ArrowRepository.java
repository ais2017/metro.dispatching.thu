package repository;

import java.util.Collection;

public interface ArrowRepository<T> {

    public void save(T t);

    public Collection<T> getAll();

    public T getOne(Integer id);

    public boolean deleteOne(Integer id);

    public void generate();

    public void clear();
}
