package repository;

import java.util.Collection;

public interface TunnelRepository<T> {
    public void save(T t);

    public Collection<T> getAll();

    public T getOne(Integer id);

    public void generate();

    public void clear();
}
