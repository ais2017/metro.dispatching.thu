package repository;

import java.util.ArrayList;
import java.util.Collection;

public interface StationRepository<T,M,N> {

    public void save(T t);

    public Collection<T> getAll();

    public T getOne(Integer id);

    public ArrayList<M> getArrowsForStation(Integer id);

    public ArrayList<N> getTrafficLightsForStation(Integer id);

    public void generate();

    public void clear();
}