package repository;

import java.util.Collection;

public interface TrainRepository<T,N,M> {

    public void save(T t);

    public Collection<T> getAll();

    public T getOne(Integer id);

    public void generate();

    public void clear();

    public N getPointOfTrainById(Integer id);

    public M getRouteOfTrainById(Integer id);
}
