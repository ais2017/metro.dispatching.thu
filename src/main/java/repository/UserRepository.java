package repository;

import java.util.Collection;

public interface UserRepository<T> {
    public void save(T t);

    public Collection<T> getAll();

    public T getOne(String name);

    public void generate();

    public void clear();
}