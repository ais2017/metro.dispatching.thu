package repository;

import java.util.Collection;

public interface RightsRepository<T> {

    public void save(T t);

    public Collection<T> getAll();

    public T getOne(String name);

    public void deleteOne(T t);

    public void generate();

    public void clear();
}
