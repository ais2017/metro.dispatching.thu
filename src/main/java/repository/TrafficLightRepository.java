package repository;

import java.util.Collection;

public interface TrafficLightRepository<T> {

    public void save(T t);

    public Collection<T> getAll();

    public Collection<T> getOne();

    public Collection<T> deleteOne();

    public void generate();

    public void clear();
}
