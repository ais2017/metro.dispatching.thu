package repository;

import java.util.Collection;

public interface RouteRepository<T> {

    public void save(T t);

    public Collection<T> getAll();

    public void generate();

    public void clear();
}
