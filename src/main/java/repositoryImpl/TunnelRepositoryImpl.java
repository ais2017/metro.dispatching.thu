package repositoryImpl;

import repository.TunnelRepository;
import type.Tunnel;

import java.util.ArrayList;
import java.util.Collection;

public class TunnelRepositoryImpl implements TunnelRepository<Tunnel> {
    private ArrayList<Tunnel> tunnelArray = new ArrayList<>();

    public TunnelRepositoryImpl() {
        this.tunnelArray = new ArrayList<>();;
    }

    @Override
    public void save(Tunnel tunnel){
        tunnelArray.add(tunnel);
    }

    @Override
    public Collection<Tunnel> getAll(){
        return tunnelArray;
    }

    @Override
    public Tunnel getOne(Integer id){
        return tunnelArray.stream().filter(iter ->
                iter.getId().equals(id)).findFirst().orElse(null);
    }

    @Override
    public void generate() {
        Tunnel tunnel1 = new Tunnel(1);
        Tunnel tunnel2 = new Tunnel(2);
        Tunnel tunnel3 = new Tunnel(3);
        Tunnel tunnel4 = new Tunnel(4);
        Tunnel tunnel5 = new Tunnel(5);
        Tunnel tunnel6 = new Tunnel(6);
        tunnelArray.add(tunnel1);
        tunnelArray.add(tunnel2);
        tunnelArray.add(tunnel3);
        tunnelArray.add(tunnel4);
        tunnelArray.add(tunnel5);
        tunnelArray.add(tunnel6);

    }

    @Override
    public void clear() {
        tunnelArray.clear();
    }

}
