package repositoryImpl;

import repository.RouteRepository;
import type.Arrow;
import type.Route;
import type.Station;
import type.TrafficLight;
import type.Tunnel;
import type.Waypoint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class RouteRepositoryImpl implements RouteRepository<Route> {
    private ArrayList<Route> routeArray = new ArrayList<>();

    public RouteRepositoryImpl() {
        this.routeArray = new ArrayList<>();;
    }

    @Override
    public void save(Route route){
        routeArray.add(route);
    }

    @Override
    public Collection<Route> getAll(){
        return routeArray;
    }

    @Override
    public void clear() {
        routeArray.clear();
    }

    @Override
    public void generate() {
        Arrow arrow1 = new Arrow(1, 1);
        Arrow arrow2 = new Arrow(2, 2);
        Arrow arrow3 = new Arrow(3, 1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1 = new TrafficLight(1, false);
        TrafficLight trafficLight2 = new TrafficLight(2, false);
        TrafficLight trafficLight3 = new TrafficLight(3, true);
        TrafficLight trafficLight4 = new TrafficLight(4, false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        Station s1 = new Station(1, arrows1, trafficLights1);
        Tunnel tt1 = new Tunnel(1);
        Waypoint w1 = new Waypoint(s1, new Date());
        Waypoint w2 = new Waypoint(tt1, new Date());
        ArrayList<Waypoint> aw1 = new ArrayList<Waypoint>();
        aw1.add(w1);
        aw1.add(w2);
        Route r1 = new Route(aw1);
        routeArray.add(r1);
    }
}
