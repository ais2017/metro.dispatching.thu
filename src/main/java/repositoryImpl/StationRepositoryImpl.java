package repositoryImpl;

import repository.StationRepository;
import type.Arrow;
import type.Station;
import type.TrafficLight;

import java.util.ArrayList;
import java.util.Collection;

public class StationRepositoryImpl implements StationRepository<Station,Arrow,TrafficLight> {
    private ArrayList<Station> stationArray = new ArrayList<>();

    public StationRepositoryImpl() {
        this.stationArray = new ArrayList<>();;
    }

    @Override
    public void save(Station station){
        stationArray.add(station);
    }

    @Override
    public Collection<Station> getAll(){
        return stationArray;
    }

    @Override
    public Station getOne(Integer id){
        return stationArray.stream().filter(iter ->
                iter.getId().equals(id)).findFirst().orElse(null);
    }

    @Override
    public ArrayList<Arrow> getArrowsForStation(Integer id){
        Station s = stationArray.stream().filter(iter ->
                iter.getId().equals(id)).findFirst().orElse(null);
        if (s==null){
            return null;
        }else{
            return s.getArrows();
        }
    }

    @Override
    public ArrayList<TrafficLight> getTrafficLightsForStation(Integer id){
        Station s =  stationArray.stream().filter(iter ->
                iter.getId().equals(id)).findFirst().orElse(null);
        if (s==null){
            return null;
        }else{
            return s.getTrafficLights();
        }
    }

    @Override
    public void clear() {
        stationArray.clear();
    }

    @Override
    public void generate() {
        Arrow arrow1=new Arrow(1,1);
        Arrow arrow2=new Arrow(2,2);
        Arrow arrow3=new Arrow(3,1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1=new TrafficLight(1,false);
        TrafficLight trafficLight2=new TrafficLight(2,false);
        TrafficLight trafficLight3=new TrafficLight(3,true);
        TrafficLight trafficLight4=new TrafficLight(4,false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        trafficLights1.add(trafficLight1);
        trafficLights1.add(trafficLight2);
        trafficLights1.add(trafficLight3);
        trafficLights1.add(trafficLight4);
        Station s1= new Station(1,arrows1,trafficLights1);
        stationArray.add(s1);
    }
}
