package repositoryImpl;

import repository.UserRepository;
import type.User;

import java.util.ArrayList;
import java.util.Collection;

public class UserRepositoryImpl implements UserRepository<User> {
    private ArrayList<User> userArray = new ArrayList<>();

    public UserRepositoryImpl() {
        this.userArray = new ArrayList<>();;
    }

    @Override
    public void save(User user){
        userArray.add(user);
    }

    @Override
    public Collection<User> getAll(){
        return userArray;
    }

    @Override
    public User getOne(String name){
        return userArray.stream().filter(iter ->
                iter.getName().equals(name)).findFirst().orElse(null);
    }

    @Override
    public void generate() {
        User user1 = new User("Main",true);
        User user2 = new User("Cool",false);
        User user3 = new User("Loh",true);
        userArray.add(user1);
        userArray.add(user2);
        userArray.add(user3);
    }

    @Override
    public void clear() {
        userArray.clear();
    }
}
