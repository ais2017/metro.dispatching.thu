package repositoryImpl;

import repository.ArrowRepository;
import type.TrafficLight;

import java.util.ArrayList;
import java.util.Collection;

public class TrafficLightRepositoryImpl implements ArrowRepository<TrafficLight> {
    private ArrayList<TrafficLight> trafficLightsArray = new ArrayList<>();

    public TrafficLightRepositoryImpl() {
        this.trafficLightsArray = new ArrayList<>();;
    }

    @Override
    public void save(TrafficLight trafficLight){
        trafficLightsArray.add(trafficLight);
    }

    @Override
    public Collection<TrafficLight> getAll(){
        return trafficLightsArray;
    }

    public TrafficLight getOne(Integer id){
        return trafficLightsArray.stream().filter(iter ->
                iter.getId().equals(id)).findFirst().orElse(null);
    }

    @Override
    public boolean deleteOne(Integer id){
        TrafficLight trafficLight = trafficLightsArray.stream().filter(iter ->
                iter.getId().equals(id)).findFirst().orElse(null);
        return trafficLightsArray.remove(trafficLight);
    }

    @Override
    public void generate() {
        TrafficLight trafficLight1=new TrafficLight(1,false);
        TrafficLight trafficLight2=new TrafficLight(2,false);
        TrafficLight trafficLight3=new TrafficLight(3,true);
        TrafficLight trafficLight4=new TrafficLight(4,false);
        trafficLightsArray.add(trafficLight1);
        trafficLightsArray.add(trafficLight2);
        trafficLightsArray.add(trafficLight3);
        trafficLightsArray.add(trafficLight4);
    }

    @Override
    public void clear() {
        trafficLightsArray.clear();
    }
}
