package repositoryImpl;

import repository.BranchRepository;
import type.Arrow;
import type.Branch;
import type.Point;
import type.Station;
import type.TrafficLight;
import type.Tunnel;

import java.util.ArrayList;
import java.util.Collection;

public class BranchRepositoryImpl implements BranchRepository<Branch,Point> {
    private ArrayList<Branch> branchArray = new ArrayList<>();

    public BranchRepositoryImpl() {
        this.branchArray = new ArrayList<>();
    }

    @Override
    public void save(Branch branch){
        branchArray.add(branch);
    }

    @Override
    public Collection<Branch> getAll(){
        return branchArray;
    }

    @Override
    public Branch getOne(Integer id){
        return branchArray.stream().filter(iter ->
                iter.getId().equals(id)).findFirst().orElse(null);
    }

    @Override
    public ArrayList<Point> getPointsOfBranch(Integer id){
        Branch b = branchArray.stream().filter(iter ->
                iter.getId().equals(id)).findFirst().orElse(null);
        if (b==null){
            return null;
        }else{
            return b.getPoints();
        }
    }

    @Override
    public void clear() {
        branchArray.clear();
    }

    @Override
    public void generate() {
        Arrow arrow1 = new Arrow(1, 1);
        Arrow arrow2 = new Arrow(2, 2);
        Arrow arrow3 = new Arrow(3, 1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        TrafficLight trafficLight1 = new TrafficLight(1, false);
        TrafficLight trafficLight2 = new TrafficLight(2, false);
        TrafficLight trafficLight3 = new TrafficLight(3, true);
        TrafficLight trafficLight4 = new TrafficLight(4, false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        Station s1 = new Station(1, arrows1, trafficLights1);
        Tunnel tt1 = new Tunnel(1);
        ArrayList<Point> p1 = new ArrayList<Point>();
        p1.add(s1);
        p1.add(tt1);
        Branch b1 = new Branch(1,p1,1);
        branchArray.add(b1);
    }
}
