package repositoryImpl;

import repository.ArrowRepository;
import type.Arrow;

import java.util.ArrayList;
import java.util.Collection;

public class ArrowRepositoryImpl implements ArrowRepository<Arrow> {
    private ArrayList<Arrow> arrowsArray = new ArrayList<>();

    public ArrowRepositoryImpl() {
        this.arrowsArray = new ArrayList<>();
    }

    @Override
    public void save(Arrow arrow){
        arrowsArray.add(arrow);
    }

    @Override
    public Collection<Arrow> getAll(){
        return arrowsArray;
    }

    @Override
    public Arrow getOne(Integer id){
        return arrowsArray.stream().filter(iter ->
                iter.getId().equals(id)).findFirst().orElse(null);

    }

    @Override
    public boolean deleteOne(Integer id){
        Arrow arrow=arrowsArray.stream().filter(iter ->
                iter.getId().equals(id)).findFirst().orElse(null);
        return arrowsArray.remove(arrow);
    }

    @Override
    public void generate() {
        Arrow arrow1 = new Arrow(1, 1);
        Arrow arrow2 = new Arrow(2, 2);
        Arrow arrow3 = new Arrow(3, 1);
        arrowsArray.add(arrow1);
        arrowsArray.add(arrow2);
        arrowsArray.add(arrow3);
    }

    @Override
    public void clear() {
        arrowsArray.clear();
    }
}
