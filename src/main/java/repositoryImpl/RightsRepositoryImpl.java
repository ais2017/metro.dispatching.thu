package repositoryImpl;

import repository.RightsRepository;
import type.Arrow;
import type.Rights;
import type.TrafficLight;
import type.User;

import java.util.ArrayList;
import java.util.Collection;

public class RightsRepositoryImpl implements RightsRepository<Rights> {
    private ArrayList<Rights> rightsArray = new ArrayList<>();

    public RightsRepositoryImpl() {
        this.rightsArray = new ArrayList<>();;
    }

    @Override
    public void save(Rights rights){
        rightsArray.add(rights);
    }

    @Override
    public void deleteOne(Rights rights){
        rightsArray.remove(rights);
    }

    @Override
    public Collection<Rights> getAll(){
        return rightsArray;
    }

    @Override
    public Rights getOne(String name){
        return rightsArray.stream().filter(iter ->
                iter.getUser().getName().equals(name)).findFirst().orElse(null);
    }


    @Override
    public void generate(){
        Arrow arrow1=new Arrow(1,1);
        Arrow arrow2=new Arrow(2,2);
        Arrow arrow3=new Arrow(3,1);
        ArrayList<Arrow> arrows1 = new ArrayList<Arrow>();
        arrows1.add(arrow1);
        arrows1.add(arrow2);
        arrows1.add(arrow3);
        ArrayList<Arrow> arrows2 = new ArrayList<Arrow>();
        arrows2.add(arrow1);
        arrows2.add(arrow2);
        ArrayList<Arrow> arrows3 = new ArrayList<Arrow>();
        arrows3.add(arrow1);

        TrafficLight trafficLight1=new TrafficLight(1,false);
        TrafficLight trafficLight2=new TrafficLight(2,false);
        TrafficLight trafficLight3=new TrafficLight(3,true);
        TrafficLight trafficLight4=new TrafficLight(4,false);
        ArrayList<TrafficLight> trafficLights1 = new ArrayList<TrafficLight>();
        trafficLights1.add(trafficLight1);
        trafficLights1.add(trafficLight2);
        trafficLights1.add(trafficLight3);
        ArrayList<TrafficLight> trafficLights2 = new ArrayList<TrafficLight>();
        trafficLights2.add(trafficLight1);
        trafficLights2.add(trafficLight2);
        ArrayList<TrafficLight> trafficLights3 = new ArrayList<TrafficLight>();
        trafficLights3.add(trafficLight4);

        rightsArray.add(new Rights(new User("Main",true),arrows1, trafficLights1));
        rightsArray.add(new Rights(new User("Cool",false),arrows2, trafficLights2));
        rightsArray.add(new Rights(new User("Loh",true),arrows3, trafficLights3));
    }

    @Override
    public void clear() {
        rightsArray.clear();
    }

}
