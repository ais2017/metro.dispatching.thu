package type;

public class Tunnel extends Point {

    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Tunnel(Integer id) {
        if (id==null)
            throw new IllegalArgumentException("Invalid params");
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tunnel tunnel = (Tunnel) o;

        return id.equals(tunnel.id);
    }

}
