package type;

import java.util.ArrayList;

public class Rights {
    private User user;
    private ArrayList<Arrow> arrows;
    private ArrayList<TrafficLight> trafficLights;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ArrayList<Arrow> getArrows() {
        return arrows;
    }

    public void setArrows(ArrayList<Arrow> arrows) {
        this.arrows = arrows;
    }

    public ArrayList<TrafficLight> getTrafficLights() {
        return this.trafficLights;
    }

    public void setTrafficLights(ArrayList<TrafficLight> trafficLights) {
        this.trafficLights = trafficLights;
    }


    public Rights(User user, ArrayList<Arrow> arrows, ArrayList<TrafficLight> trafficLights) {
        if (user==null || arrows==null || trafficLights==null)
            throw new IllegalArgumentException("Invalid params");
        this.user = user;
        this.arrows = arrows;
        this.trafficLights = trafficLights;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rights rights = (Rights) o;

        return user.equals(rights.user);
    }

    @Override
    public int hashCode() {
        return user.hashCode();
    }
}
