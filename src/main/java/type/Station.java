package type;

import java.util.ArrayList;

public class Station extends Point {

    private Integer id;
    private ArrayList <Arrow> arrows;
    private ArrayList <TrafficLight> trafficLights;
    private ArrayList <Platform> platforms;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<Arrow> getArrows() {
        return arrows;
    }

    public void setArrows(ArrayList<Arrow> arrows) {
        this.arrows = arrows;
    }

    public ArrayList<TrafficLight> getTrafficLights() {
        return trafficLights;
    }

    public void setTrafficLights(ArrayList<TrafficLight> trafficLights) {
        this.trafficLights = trafficLights;
    }

    public ArrayList<Platform> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(ArrayList<Platform> platforms) {
        this.platforms = platforms;
    }

    public void addArrow (Arrow arrow){
        if (arrow!=null){
            arrows.add(arrow);
        }else{
            throw new IllegalArgumentException("Invalid parameter");
        }
    }

    public void addTrafficLight (TrafficLight trafficLight){
        if (trafficLight!=null){
            trafficLights.add(trafficLight);
        }else{
            throw new IllegalArgumentException("Invalid parameter");
        }
    }


    public Station(ArrayList<Arrow> arrows, ArrayList<TrafficLight> trafficLights,ArrayList <Platform> platforms) {
        if (id==null || arrows==null || trafficLights==null || platforms==null)
            throw new IllegalArgumentException("Invalid params");
        this.id=id;
        this.arrows = arrows;
        this.trafficLights = trafficLights;
        this.platforms = platforms;
    }

    public Station(Integer id,ArrayList<TrafficLight> trafficLights) {
        if (id==null || arrows==null)
            throw new IllegalArgumentException("Invalid params");
        this.id=id;
        this.trafficLights = trafficLights;
    }


    public Station(Integer id, ArrayList<Arrow> arrows, ArrayList<TrafficLight> trafficLights) {
        if (id==null || arrows==null || trafficLights==null)
            throw new IllegalArgumentException("Invalid params");
        this.id=id;
        this.arrows = arrows;
        this.trafficLights = trafficLights;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Station station = (Station) o;

        return id.equals(station.id);
    }
}
