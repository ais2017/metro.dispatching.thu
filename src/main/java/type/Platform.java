package type;

public class Platform {

    private Integer numb;

    public Integer getNumb() {
        return numb;
    }

    public void setNumb(Integer numb) {
        this.numb = numb;
    }

    public Platform(Integer numb) {
        if (numb == null  || numb <= 0 || numb > 6)
            throw new IllegalArgumentException("Invalid params");
        this.numb = numb;
    }
}
