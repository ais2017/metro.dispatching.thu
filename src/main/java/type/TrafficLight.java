package type;

public class TrafficLight {

    private Integer id;
    private Boolean state;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean isState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public TrafficLight(Integer id, Boolean state) {
        if (id==null || state==null)
            throw new IllegalArgumentException("Invalid params");
        this.id = id;
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrafficLight that = (TrafficLight) o;

        return id.equals(that.id);
    }

}
