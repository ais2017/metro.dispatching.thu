package type;

import java.util.ArrayList;

public class Branch {

    private Integer id;
    private ArrayList<Point> points;
    private Integer typeBranch;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<Point> getPoints() {
        return points;
    }

    public void setPoints(ArrayList<Point> points) {
        this.points = points;
    }

    public Integer getTypeBranch() {
        return typeBranch;
    }

    public void setTypeBranch(Integer typeBranch) {
        this.typeBranch = typeBranch;
    }

    public Branch(Integer id,ArrayList<Point> points, Integer typeBranch) {
        if (id==null || points == null || typeBranch==null || typeBranch <= 0 || typeBranch > 3)
            throw new IllegalArgumentException("Invalid params");
        this.id = id;
        this.points = points;
        this.typeBranch = typeBranch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Branch branch = (Branch) o;

        return id.equals(branch.id);
    }
}
