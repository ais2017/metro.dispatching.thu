package type;

public class Location {

    private Point point;

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public Location(Station point) {
        if (point == null)
            throw new IllegalArgumentException("Invalid params");
        this.point = point;
    }

    public Location(Tunnel point) {
        if (point == null)
            throw new IllegalArgumentException("Invalid params");
        this.point = point;
    }
}
