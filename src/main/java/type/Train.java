package type;

public class Train {

    private Integer id;
    private Location location;
    private Route route;
    private Integer typeOfTrain;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Integer getTypeOfTrain() {
        return typeOfTrain;
    }

    public void setTypeOfTrain(Integer typeOfTrain) {
        this.typeOfTrain = typeOfTrain;
    }

    public Train(Integer id,Location location, Route route, Integer typeOfTrain) {
        if (id==null || location==null || route==null || typeOfTrain==null || typeOfTrain <= 0 || typeOfTrain > 3)
            throw new IllegalArgumentException("Invalid params");
        this.id=id;
        this.location = location;
        this.route = route;
        this.typeOfTrain = typeOfTrain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Train train = (Train) o;

        return id.equals(train.id);
    }

}
