package type;

public class Arrow {

    private Integer id;
    private Integer direction;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public Arrow(Integer id, Integer direction) {
        if (id==null || direction == null || direction <= 0 || direction > 3)
            throw new IllegalArgumentException("Invalid params");
        this.id = id;
        this.direction = direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Arrow arrow = (Arrow) o;

        return id.equals(arrow.id);
    }

}
