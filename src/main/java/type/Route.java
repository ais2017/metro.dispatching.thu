package type;

import java.util.ArrayList;

public class Route {

    private ArrayList<Waypoint> waypoints;

    public ArrayList<Waypoint> getWaypoints() {
        return waypoints;
    }



    public void setWaypoints(ArrayList<Waypoint> waypoints) {
        this.waypoints = waypoints;
    }

    public void addWaypoint (Waypoint waypoint){
        if (waypoint!=null){
            waypoints.add(waypoint);
        }else{
            throw new IllegalArgumentException("Invalid parameter");
        }
    }

    public void deleteWaypoint (Waypoint waypoint){
        if (waypoint!=null){
            waypoints.remove(waypoint);
        }else{
            throw new IllegalArgumentException("Invalid parameter");
        }
    }

    public Route(ArrayList<Waypoint> waypoints) {
        if (waypoints==null)
            throw new IllegalArgumentException("Invalid params");
        this.waypoints = waypoints;
    }
}
