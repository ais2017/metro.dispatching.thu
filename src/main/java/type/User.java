package type;

public class User {

    private String name;
    private Boolean readyToReplacement;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isReadyToReplacement() {
        return readyToReplacement;
    }

    public void setReadyToReplacement(boolean readyToReplacement) {
        this.readyToReplacement = readyToReplacement;
    }

    public User(String name, Boolean readyToReplacement) {
        if (readyToReplacement == null)
            throw new IllegalArgumentException("Invalid params");
        this.name = name;
        this.readyToReplacement = readyToReplacement;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return name.equals(user.name);
    }

}
