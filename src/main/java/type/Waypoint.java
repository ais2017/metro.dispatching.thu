package type;

import java.util.Date;

public class Waypoint {

    private Point point;
    private Date timeOfPoint;

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

    public Date getTimeOfPoint() {
        return timeOfPoint;
    }

    public void setTimeOfPoint(Date timeOfPoint) {
        this.timeOfPoint = timeOfPoint;
    }

    public Waypoint(Point point, Date timeOfPoint) {
        if (point==null || timeOfPoint==null)
            throw new IllegalArgumentException("Invalid params");
        this.point = point;
        this.timeOfPoint = timeOfPoint;
    }
}
