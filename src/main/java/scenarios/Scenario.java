package scenarios;

import repositoryImpl.ArrowRepositoryImpl;
import repositoryImpl.BranchRepositoryImpl;
import repositoryImpl.RightsRepositoryImpl;
import repositoryImpl.RouteRepositoryImpl;
import repositoryImpl.StationRepositoryImpl;
import repositoryImpl.TrafficLightRepositoryImpl;
import repositoryImpl.TrainRepositoryImpl;
import repositoryImpl.TunnelRepositoryImpl;
import repositoryImpl.UserRepositoryImpl;
import type.Arrow;
import type.Point;
import type.Rights;
import type.Route;
import type.Station;
import type.TrafficLight;
import type.Tunnel;
import type.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Scenario {

    public ArrowRepositoryImpl arrowRepositoryImpl;
    public BranchRepositoryImpl branchRepositoryImpl;
    public RightsRepositoryImpl rightsRepositoryImpl;
    public RouteRepositoryImpl routeRepositoryImpl;
    public StationRepositoryImpl stationRepositoryImpl;
    public TrafficLightRepositoryImpl trafficLightRepositoryImpl;
    public TrainRepositoryImpl trainRepository;
    public TunnelRepositoryImpl tunnelRepositoryImpl;
    public UserRepositoryImpl userRepositoryImpl;

    public Scenario(ArrowRepositoryImpl arrowRepositoryImpl, BranchRepositoryImpl branchRepositoryImpl, RightsRepositoryImpl rightsRepositoryImpl, RouteRepositoryImpl routeRepositoryImpl, StationRepositoryImpl stationRepositoryImpl, TrafficLightRepositoryImpl trafficLightRepositoryImpl, TrainRepositoryImpl trainRepository, TunnelRepositoryImpl tunnelRepositoryImpl, UserRepositoryImpl userRepositoryImpl) {
        this.arrowRepositoryImpl = arrowRepositoryImpl;
        this.branchRepositoryImpl = branchRepositoryImpl;
        this.rightsRepositoryImpl = rightsRepositoryImpl;
        this.routeRepositoryImpl = routeRepositoryImpl;
        this.stationRepositoryImpl = stationRepositoryImpl;
        this.trafficLightRepositoryImpl = trafficLightRepositoryImpl;
        this.trainRepository = trainRepository;
        this.tunnelRepositoryImpl = tunnelRepositoryImpl;
        this.userRepositoryImpl = userRepositoryImpl;
    }

    public void setDirectionOfArrow(Integer idArrow, String nameOfUser, Integer direction) { //+
        if (idArrow == null || nameOfUser == null || direction == null)
            throw new IllegalArgumentException("Invalid params");
        if (userRepositoryImpl.getOne(nameOfUser) == null) {
            throw new IllegalArgumentException("Нет такого пользователя");
        }
        if (arrowRepositoryImpl.getOne(idArrow) == null) {
            throw new IllegalArgumentException("Нет такой стрелки");
        }
        Rights rightsForUser = rightsRepositoryImpl.getOne(nameOfUser);
        Arrow arrow = arrowRepositoryImpl.getOne(idArrow);
        ArrayList<Arrow>  arrowsForUser = rightsForUser.getArrows();
        Arrow s = arrowsForUser.stream().filter(iter ->
                    iter.getId().equals(idArrow)).findFirst().orElse(null);
            if (s!=null) {
                s.setDirection(direction);
                System.out.println("Стрелка с идентификатором=" + idArrow + " переключена на направление " + direction);
            } else {
                System.out.println("Нет прав на эту стрелку");
            }
    }

    public void setStateOfTrafficLight(Integer idTrafficLight, String nameOfUser, Boolean state) { //+
        if (idTrafficLight == null || nameOfUser == null || state == null)
            throw new IllegalArgumentException("Invalid params");
        if (userRepositoryImpl.getOne(nameOfUser)==null){
            throw new IllegalArgumentException("Нет такого пользователя");
        }
        if (trafficLightRepositoryImpl.getOne(idTrafficLight)==null){
            throw new IllegalArgumentException("Нет такой стрелки");
        }
        Rights rightsForUser = rightsRepositoryImpl.getOne(nameOfUser);
        TrafficLight trafficLight = trafficLightRepositoryImpl.getOne(idTrafficLight);
        ArrayList<TrafficLight>  trafficLightsForUser = rightsForUser.getTrafficLights();
        if (trafficLightsForUser.stream().filter(iter ->
                iter.getId().equals(idTrafficLight)).findFirst().get().equals(trafficLight)) {
            trafficLight.setState(state);
            System.out.println("Светофор с идентификатором=" + idTrafficLight + " переключен в состояние " + state);

        } else {
            System.out.println("Нет прав на этот светофор");
        }
    }

    public void deleteRightsForArrowForUser(String name,Integer idArrow){ //+
        if (idArrow == null || name == null)
            throw new IllegalArgumentException("Invalid params");
        if (userRepositoryImpl.getOne(name)==null){
            throw new IllegalArgumentException("Нет такого пользователя");
        }
        if (arrowRepositoryImpl.getOne(idArrow)==null){
            throw new IllegalArgumentException("Нет такой стрелки");
        }
        Rights rightsForUser = rightsRepositoryImpl.getOne(name);
        ArrayList<TrafficLight> trafficLightsForUser = rightsForUser.getTrafficLights();
        ArrayList<Arrow> arrowsForUser = rightsForUser.getArrows();
        User user = userRepositoryImpl.getOne(name);
        arrowsForUser.remove(arrowRepositoryImpl.getOne(idArrow));
        Rights rightsForUserNew = new Rights(user,arrowsForUser,trafficLightsForUser);
        rightsRepositoryImpl.deleteOne(rightsForUser);
        rightsRepositoryImpl.save(rightsForUserNew);
        System.out.println("Права изменены");
    }

    public void deleteRightsForTrafficLightForUser(String name,Integer idTrafficLight){ //+
        if (idTrafficLight == null || name == null)
            throw new IllegalArgumentException("Invalid params");
        if (userRepositoryImpl.getOne(name)==null){
            throw new IllegalArgumentException("Нет такого пользователя");
        }
        if (trafficLightRepositoryImpl.getOne(idTrafficLight)==null){
            throw new IllegalArgumentException("Нет такой стрелки");
        }
        Rights rightsForUser = rightsRepositoryImpl.getOne(name);
        TrafficLight trafficLight = trafficLightRepositoryImpl.getOne(idTrafficLight);
        ArrayList<TrafficLight> trafficLightsForUser = rightsForUser.getTrafficLights();
        ArrayList<Arrow> arrowsForUser = rightsForUser.getArrows();
        User user = userRepositoryImpl.getOne(name);
        trafficLightsForUser.remove(trafficLightRepositoryImpl.getOne(idTrafficLight));
        Rights rightsForUserNew = new Rights(user,arrowsForUser,trafficLightsForUser);
        rightsRepositoryImpl.deleteOne(rightsForUser);
        rightsRepositoryImpl.save(rightsForUserNew);
        System.out.println("Права изменены");
    }

    public void addRightsForArrowForUser(String name,Integer idArrow){   //+
        if (idArrow == null || name == null)
            throw new IllegalArgumentException("Invalid params");
        if (userRepositoryImpl.getOne(name)==null){
            throw new IllegalArgumentException("Нет такого пользователя");
        }
        if (arrowRepositoryImpl.getOne(idArrow)==null){
            throw new IllegalArgumentException("Нет такой стрелки");
        }
        Rights rightsForUser = rightsRepositoryImpl.getOne(name);
        Arrow arrow = arrowRepositoryImpl.getOne(idArrow);
        ArrayList<TrafficLight> trafficLightsForUser = rightsForUser.getTrafficLights();
        ArrayList<Arrow> arrowsForUser = rightsForUser.getArrows();
        User user = userRepositoryImpl.getOne(name);
        arrowsForUser.add(arrowRepositoryImpl.getOne(idArrow));
        Rights rightsForUserNew = new Rights(user,arrowsForUser,trafficLightsForUser);
        rightsRepositoryImpl.deleteOne(rightsForUser);
        rightsRepositoryImpl.save(rightsForUserNew);
        System.out.println("Права изменены");
    }

    public void addRightsForTrafficLightForUser(String name,Integer idTrafficLight){  //+
        if (idTrafficLight == null || name == null)
            throw new IllegalArgumentException("Invalid params");
        if (userRepositoryImpl.getOne(name)==null){
            throw new IllegalArgumentException("Нет такого пользователя");
        }
        if (trafficLightRepositoryImpl.getOne(idTrafficLight)==null){
            throw new IllegalArgumentException("Нет такой стрелки");
        }
        Rights rightsForUser = rightsRepositoryImpl.getOne(name);
        TrafficLight trafficLight = trafficLightRepositoryImpl.getOne(idTrafficLight);
        ArrayList<TrafficLight> trafficLightsForUser = rightsForUser.getTrafficLights();
        ArrayList<Arrow> arrowsForUser = rightsForUser.getArrows();
        User user = userRepositoryImpl.getOne(name);
        trafficLightsForUser.add(trafficLightRepositoryImpl.getOne(idTrafficLight));
        Rights rightsForUserNew = new Rights(user,arrowsForUser,trafficLightsForUser);
        rightsRepositoryImpl.deleteOne(rightsForUser);
        rightsRepositoryImpl.save(rightsForUserNew);
        System.out.println("Права изменены");
    }


    public  void addTrafficLight(Integer idTrafficLight, Boolean state) { //+
        if (idTrafficLight == null || state == null)
            throw new IllegalArgumentException("Invalid params");
        if (trafficLightRepositoryImpl.getOne(idTrafficLight) != null) {
            throw new IllegalArgumentException("Уже существует светофор с таким идентификатором");
        } else {
            TrafficLight trafficLight = new TrafficLight(idTrafficLight, state);
            trafficLightRepositoryImpl.save(trafficLight);
            System.out.println("Светофор добавлен");
        }
    }

    public  void addArrow(Integer idArrow, Integer direction) { //+
        if (idArrow == null || direction == null)
            throw new IllegalArgumentException("Invalid params");
        if (arrowRepositoryImpl.getOne(idArrow) != null) {
            throw new IllegalArgumentException("Уже существует стрелка с таким идентификатором");
        } else {
            Arrow arrow = new Arrow(idArrow, direction);
            arrowRepositoryImpl.save(arrow);
            System.out.println("Стрелка добавлена");
        }
    }

    public  boolean deleteTrafficLight(Integer idTrafficLight){ //+
        if (idTrafficLight == null)
            throw new IllegalArgumentException("Invalid params");
        if (trafficLightRepositoryImpl.getOne(idTrafficLight)==null){
            throw new IllegalArgumentException("Не существует светофора с таким идентификатором");
        }else{
            return trafficLightRepositoryImpl.deleteOne(idTrafficLight);
        }
    }

    public  boolean deleteArrow(Integer idArrow){ //+
        if (idArrow == null)
            throw new IllegalArgumentException("Invalid params");
        if (arrowRepositoryImpl.getOne(idArrow)==null){
            throw new IllegalArgumentException("Не существует стрелки с таким идентификатором");
        }else{
            return arrowRepositoryImpl.deleteOne(idArrow);
        }
    }

    public  void getDirectionsOfArrowsAroundBranch(Integer idBranch) { //+
        List<Arrow> resultSetOfArrows = new ArrayList<Arrow>();
        if (idBranch == null)
            throw new IllegalArgumentException("Invalid params");
        if (branchRepositoryImpl.getOne(idBranch) == null) {
            throw new IllegalArgumentException("Не существует такой ветки");
        } else {
            Collection<Point> pointsOfBranch = branchRepositoryImpl.getPointsOfBranch(idBranch);
            for (Point p : pointsOfBranch) {
                if (p instanceof Station) {
                    Station stationOfBranch = (Station) p;
                    resultSetOfArrows.addAll(stationRepositoryImpl.getArrowsForStation(stationOfBranch.getId()));
                }else{
                    continue;
                }
            }
            List<Integer> idsArrows = resultSetOfArrows.stream().map(Arrow::getId).collect(Collectors.toList());
            for (Integer idd : idsArrows) {
                if(arrowRepositoryImpl.getOne(idd)!=null){
                System.out.println("Стрелка с идентификатором = " + idd + " имеет направление " + arrowRepositoryImpl.getOne(idd).getDirection());
            }else{
                    continue;
                }
            }
        }
    }


    public  void getStateOfTrafficLightsAroundBranch(Integer idBranch) { //+
        String result;
        List<TrafficLight> resultSetOfTrafficLights = new ArrayList<TrafficLight>();
        if (idBranch == null)
            throw new IllegalArgumentException("Invalid params");
        if (branchRepositoryImpl.getOne(idBranch) == null) {
            throw new IllegalArgumentException("Не существует такой ветки");
        } else {
            Collection <Point> pointsOfBranch= branchRepositoryImpl.getPointsOfBranch(idBranch);
            for (Point p : pointsOfBranch) {
                if (p instanceof Station){
                    Station stationOfBranch = (Station) p;
                    resultSetOfTrafficLights.addAll(stationRepositoryImpl.getTrafficLightsForStation(stationOfBranch.getId()));
                }else{
                    continue;
                }
            }
            List<Integer> idsTrafficLights = resultSetOfTrafficLights.stream().map(TrafficLight::getId).collect(Collectors.toList());
            for (Integer idd : idsTrafficLights) {
                if (trafficLightRepositoryImpl.getOne(idd)!=null){
                    if (trafficLightRepositoryImpl.getOne(idd).isState()==true){
                        result=" включен";
                    }else{
                        result=" выключен";
                    }
                    System.out.println("Светофор с идентификатором = "+idd+" имеет состояние"+result);
                }else {
                    continue;
                }

            }
        }
    }

        public  ArrayList<Arrow> getArrowsAroundStation (Integer idStation){ //+
        ArrayList<Arrow> resultSetOfArrows = new ArrayList<Arrow>();
            if (idStation == null)
                throw new IllegalArgumentException("Invalid params");
            if (stationRepositoryImpl.getOne(idStation) == null) {
                throw new IllegalArgumentException("Не существует такой станции");
            } else {
                if(stationRepositoryImpl.getArrowsForStation(idStation)!=null){
                    return stationRepositoryImpl.getArrowsForStation(idStation);
                }else{
                    return null;
                }

            }
        }

        public  ArrayList<TrafficLight> getTrafficLightAroundStation (Integer idStation){ //+
            ArrayList<TrafficLight> resultSetOfTrafficLights = new ArrayList<TrafficLight>();
            String result;
            if (idStation == null)
                throw new IllegalArgumentException("Invalid params");
            if (stationRepositoryImpl.getOne(idStation) == null) {
                throw new IllegalArgumentException("Не существует такой станции");
            } else {
                if (stationRepositoryImpl.getTrafficLightsForStation(idStation)!=null){
                    return resultSetOfTrafficLights = stationRepositoryImpl.getTrafficLightsForStation(idStation);
                }else{
                    return null;
                }
             }
        }

    public  void getDirectionOfArrowsAroundStation(Integer idStation) { //+
        if (idStation == null)
            throw new IllegalArgumentException("Invalid params");
        if (stationRepositoryImpl.getOne(idStation) == null) {
            throw new IllegalArgumentException("Не существует такой станции");
        } else {
            Collection<Arrow> arrowsAroundStation = stationRepositoryImpl.getArrowsForStation(idStation);
            List<Integer> idsArrows = arrowsAroundStation.stream().map(Arrow::getId).collect(Collectors.toList());
            for (Integer idd : idsArrows) {
                System.out.println("Стрелка с идентификатором = "+idd+" имеет направление "+ arrowRepositoryImpl.getOne(idd).getDirection());
            }
        }
    }

    public  void getStateOfTrafficLightAroundStation(Integer idStation) { //+
        String result;
        if (idStation == null)
            throw new IllegalArgumentException("Invalid params");
        if (stationRepositoryImpl.getOne(idStation) == null) {
            throw new IllegalArgumentException("Не существует такой станции");
        } else {
            if(stationRepositoryImpl.getTrafficLightsForStation(idStation)!=null){
                Collection<TrafficLight> trafficLightsAroundStation = stationRepositoryImpl.getTrafficLightsForStation(idStation);
                List<Integer> idsTrafficLights = trafficLightsAroundStation.stream().map(TrafficLight::getId).collect(Collectors.toList());
                for (Integer idd : idsTrafficLights) {
                    if (trafficLightRepositoryImpl.getOne(idd)!=null){
                        if (trafficLightRepositoryImpl.getOne(idd).isState()==true){
                            result=" включен";
                        }else{
                            result=" выключен";
                        }
                        System.out.println("Светофор с идентификатором = "+idd+" имеет состояние"+result);
                    }else{
                        continue;
                    }
            }
            }else{
                throw new IllegalArgumentException("Нет светофоров для этой станции");
            }
        }
    }

    public  Integer getDirectionOfArrow(Integer idArrow) { //+
        if (idArrow == null)
            throw new IllegalArgumentException("Invalid params");
        if (arrowRepositoryImpl.getOne(idArrow) == null) {
            throw new IllegalArgumentException("Не существует такой станции");
        } else {
            return arrowRepositoryImpl.getOne(idArrow).getDirection();
        }
    }

    public  Boolean getStateOfTrafficLight(Integer idTrafficLight) { //+
        if (idTrafficLight == null)
            throw new IllegalArgumentException("Invalid params");
        if (trafficLightRepositoryImpl.getOne(idTrafficLight) == null) {
            throw new IllegalArgumentException("Не существует такой станции");
        } else {
            return trafficLightRepositoryImpl.getOne(idTrafficLight).isState();
        }
    }

    public  Point getLocationOfTrain(Integer idTrain) { //+
        if (idTrain == null)
            throw new IllegalArgumentException("Invalid params");
        if (trainRepository.getPointOfTrainById(idTrain) == null) {
            throw new IllegalArgumentException("Не существует такого поезда");
        } else {
            return trainRepository.getPointOfTrainById(idTrain);
        }
    }

    public  Route getRouteOfTrain(Integer idTrain) { //+
        if (idTrain == null)
            throw new IllegalArgumentException("Invalid params");
        if (trainRepository.getRouteOfTrainById(idTrain) == null) {
            throw new IllegalArgumentException("Не существует такого поезда");
        } else {
            return trainRepository.getRouteOfTrainById(idTrain);
        }
    }

    public  Boolean getWorkloadOfTunnel(Integer idTunnel) { //+
        if (idTunnel == null)
            throw new IllegalArgumentException("Invalid params");
        if (tunnelRepositoryImpl.getOne(idTunnel) == null) {
            throw new IllegalArgumentException("Не существует такого туннеля");
        } else {
            if (trainRepository.getPointOfTrainById(idTunnel)!=null){
                return true;
            }else{
                return false;
            }
        }
    }

    public void getWorkloadOfAllTunnels() { //+
        String result;
        if (tunnelRepositoryImpl.getAll()!=null) {
            Collection<Tunnel> allTunnels = tunnelRepositoryImpl.getAll();
            List<Integer> ids = allTunnels.stream().map(Tunnel::getId).collect(Collectors.toList());
            for (Integer idd : ids) {
                if (trainRepository.getPointOfTrainById(idd) != null) {
                    result = " загружен";
                } else {
                    result = " свободен";
                }
                System.out.println("Туннель с идентификатором = " + idd + result);
            }
        }else{
            System.out.println("Туннелей не существует");
        }
        }

}
